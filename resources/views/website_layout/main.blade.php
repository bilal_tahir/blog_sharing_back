<!DOCTYPE html>
<html lang="en" class="wide wow-animation">

<head>
    <title>@yield('title')</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="{{asset('webasset/web/images/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="{{asset('webasset/web/css/css.css?family=Montserrat:400,700%7CLato:300,300italic,400,400italic,700,900%7CPlayfair+Display:700italic,900')}}">
    <link rel="stylesheet" href="{{asset('webasset/web/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('webasset/web/css/video_modal.css')}}">

    	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CMuli:400,700" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="{{asset('blogasset/css/bootstrap.min.css')}}"/>

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="{{asset('blogasset/css/font-awesome.min.css')}}">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="{{asset('blogasset/css/style.css')}}" />
    @yield('page_level_css_plugin')
    @yield('page_level_css')
    <style>
        .amcharts-chart-div>a {
            display: none !important;
        }

        #ticker01 li>a {
            color: #d37252 !important;
        }

        .top-heading-line {
            padding: 15px !important;
            font-size: 22px;
            font-weight: 600;
            color: #eac2b5;
        }

        @font-face {
            font-family: "SF-Movie-Poster-Oblique";
            src: url("{{asset('webasset/web/fonts/sf-movie-poster/SF-Movie-Poster-Oblique.ttf')}}") format("truetype");
        }

        @font-face {
            font-family: "SF-Movie-Poster-Condensed-Oblique";
            src: url("{{asset('webasset/web/fonts/sf-movie-poster/SF-Movie-Poster-Condensed-Oblique.ttf')}}") format("truetype");
        }

        @font-face {
            font-family: "SF-Movie-Poster-Condensed-Bold";
            src: url("{{asset('webasset/web/fonts/sf-movie-poster/SF-Movie-Poster-Condensed-Bold.ttf')}}") format("truetype");
        }

        @font-face {
            font-family: "SF-Movie-Poster-Condensed";
            src: url("{{asset('webasset/web/fonts/sf-movie-poster/SF-Movie-Poster-Condensed.ttf')}}") format("truetype");
        }

        @font-face {
            font-family: "SF-Movie-Poster-Bold-Oblique";
            src: url("{{asset('webasset/web/fonts/sf-movie-poster/SF-Movie-Poster-Bold-Oblique.ttf')}}") format("truetype");
        }

        @font-face {
            font-family: "SF-Movie-Poster-Bold";
            src: url("{{asset('webasset/web/fonts/sf-movie-poster/SF-Movie-Poster-Bold.ttf')}}") format("truetype");
        }

        @font-face {
            font-family: "SF-Movie-Poster";
            src: url("{{asset('webasset/web/fonts/sf-movie-poster/SF-Movie-Poster.ttf')}}") format("truetype");
        }

        .SF-Movie-Poster {
            font-family: "Sf-Movie-Poster";
        }

        .SF-Movie-Poster-Bold {
            font-family: "SF-Movie-Poster-Bold";
        }

        .SF-Movie-Poster-Bold-Oblique {
            font-family: "SF-Movie-Poster-Bold-Oblique";
        }

        .SF-Movie-Poster-Condensed {
            font-family: "SF-Movie-Poster-Condensed";
        }

        .SF-Movie-Poster-Condensed-Bold {
            font-family: "SF-Movie-Poster-Condensed-Bold";
        }

        .SF-Movie-Poster-Condensed-Oblique {
            font-family: "SF-Movie-Poster-Condensed-Oblique";
        }

        .SF-Movie-Poster-Oblique {
            font-family: "SF-Movie-Poster-Oblique";
        }
    </style>
    <link href="//vjs.zencdn.net/7.0/video-js.min.css" rel="stylesheet">
    <script src="//vjs.zencdn.net/7.0/video.min.js"></script>
</head>

<body style="" class="">
    <br>
    <div class="page">
        @include('website_layout.header')
        <main class="page-content">
            <section class="bg-gray-base text-center">
                <div>
                    <p class="top-heading-line SF-Movie-Poster-Bold" style="color: antiquewhite; font-size:45px;line-height: initial">
                        R e d i s t r i b u t i n g &nbsp R e s e a r c h
                    </p>
                </div>
            </section>
            @yield('content')
        </main>
        <!-- @include('website_layout.footer_main') -->
        @include('website_layout.footer')
    </div>
    <script src="{{asset('webasset/web/js/core.min.js')}}"></script>
    <script src="{{asset('webasset/web/js/script.js')}}"></script>
    <script src="{{asset('webasset/web/js/video_modal.js')}}"></script>
    <script src="{{asset('blogsset/js/jquery.min.js')}}"></script>
	<script src="{{asset('blogsset/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('blogsset/js/jquery.stellar.min.js')}}"></script>
	<script src="{{asset('blogsset/js/main.js')}}"></script>

    @yield('page_level_js_plugin')
    @yield('page_level_js')
</body>

</html>
