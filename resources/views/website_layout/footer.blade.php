<footer class="page-foot section-15 bg-gray-base">
    <div class="shell text-center">
        <div class="range">
            <div class="cell-sm-12">
                <p class="rights text-white" style="color: antiquewhite"><span>&#169;&nbsp;</span><span id="copyright-year"></span><span><b><i>Sharingback</i></b>&nbsp; | &nbsp Redistributing Research </span>
                </p>
            </div>
        </div>
    </div>
</footer>
