<header class="page-head">
    <div class="rd-navbar-wrap" style="height:0px !important">
        <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-stick-up-clone="false" data-md-stick-up-offset="53px" data-lg-stick-up-offset="53px" data-md-stick="true" data-lg-stick-up="true" class="rd-navbar rd-navbar-corporate-light" style="background-color:#c38c42">
            <div class="rd-navbar-inner">

                <div class="rd-navbar-group">
                    <div class="rd-navbar-panel">
                        <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
                        <a href="{{url('/')}}" class="rd-navbar-brand brand logo"><img src="{{asset('webasset/web/images/logo-sharingback.png')}}" width="120px" alt="logo" /></a>
                    </div>
                    <h3 style="color: antiquewhite; padding-left: 40px; margin-top:10px; line-height:0.75;text-align:-webkit-center;font-size: 80px;" class="SF-Movie-Poster-Bold head"> <a href="{{url('/')}}"> Sharingback </a><br>
                        <!-- <span style="font-size: initial;"> Redistributing Research </span>  -->
                    </h3>
                    <div class="rd-navbar-nav-wrap">
                        <div class="rd-navbar-nav-inner">
                            <ul class="rd-navbar-nav">
                                <li><a class="tex" href="{{url('/')}}">Home</a> </li>
                                <li><a class="tex" href="{{url('about')}}">About</a></li>
                                <li><a class="tex" href="{{url('projects')}}">Projects</a></li>
                                <li><a class="tex" href="{{url('events')}}">Events</a></li>
                                <li><a class="tex" href="{{url('team')}}">Team</a></li>
                                <li><a class="tex" href="{{url('contactUs')}}">Contact Us</a></li>
                                <li><a style="font-size: 28px" href="https://www.facebook.com/Sharingback/" class="fa fa-facebook"></a></li>
                                <!-- <li class="active">
                                    <a data-caption-animate="fadeInUp" data-caption-delay="250" href="#" class="btn btn-xl btn-primary-contrast" style="border: 1px solid #000; border-bottom:0px solid #fff">Work with Us</a>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
