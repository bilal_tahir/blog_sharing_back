// svg path for target icon
var targetSVG =
    "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";

var map = AmCharts.makeChart("chartdiv", {
    type: "map",
    theme: "light",
    showDescriptionOnHover: false,
    dataProvider: {
        map: "worldLow",
        images: [
            {
                color: "#000000",
                svgPath: targetSVG,
                label: " ",
                latitude: -24.4903019,
                longitude: 14.888755,
                scale: 1.5,
                title: "Kuboes, South Africa.",
                description: `<section id="section-start-journey" class="section-50 section-sm-90 section-lg-top-40 section-lg-bottom-50">
                                <div class="shell">
                                    <div class="range range-md-top range-sm-center">
                                       <div data-wow-duration="1s" data-wow-offset="200" class="holder cell-sm-10 cell-md-3 wow  bg-gray-lighter">


                                        </div>
                                        <div class="cell-sm-10 cell-md-3">
                                            <!-- <h2 class="text-red-orange">Most Flexible w</h2>
                                            <p class="h3 offset-top-0">Bootstrap theme</p> -->
                                            <p class="offset-top-0 text-secondary ">
                                                Film project on the oral history of the Richtersveld world heritage site Northern Cape, South Africa.
                                            </p>
                                            <p class="offset-top-10 text-secondary">
                                       For project information Kuboes, South Africa, please contact <a href="mailto:info@sharingback.org">info@sharingback.org</a>
                                        </p>
                                            <!-- <a href="#" hiddenclass="btn btn-xl btn-primary">get starbis now</a> -->
                                        </div>
                                        <div class="cell-sm-11 cell-md-6 offset-top-40 offset-sm-top-60 offset-md-top-0">
                                        <div class="image-group" style="display:block;">
                                                <h4>Film Funded By</h4>

                                <img class="logo_set" src="../img/logo-6/pic5_2.jpeg" style="height: 150px !important; width: 450px !important;" alt="">

                                                <h4>Project Partners</h4>
                                            <img style="width:160px !important" src="../img/logo-6/municipality.jpg">

                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </section>`
            },
            {
                color: "#000000",
                svgPath: targetSVG,
                label: "",
                latitude: -33.0026651,
                longitude: 21.0955628,
                scale: 1.5,
                title: "Richtersveld, South Africa.",
                description: `<section id="section-start-journey" class="section-50 section-sm-90 section-lg-top-40 section-lg-bottom-50">
                                <div class="shell">
                                    <div class="range range-md-top range-sm-center">
                                     <div data-wow-duration="1s" data-wow-offset="200" class="holder cell-sm-10 cell-md-3 wow  bg-gray-lighter">
                                        <button data-modal="#modal-html5-video-6" class="btn_set11">
                                            <img style="width:100% !important; height:100% !important" src="../img/thumb/thumb_9.png">
                                        </button>
                                        </div>
                                        <div class="cell-sm-10 cell-md-4">
                                            <!-- <h2 class="text-red-orange">Most Flexible w</h2>
                                            <p class="h3 offset-top-0">Bootstrap theme</p> -->
                                            <p class="h5 offset-top-0">Marginalised herders: Social dynamics and natural resource use in the fragile environment of the Richtersveld National Park, South Africa
                            </p>

                                            <p class="offset-top-10 text-secondary text-justify">
                     <u>Lena M. Michler</u>; Anna C. Treytde; <u>Humair Hayat</u>; Stefanie Lemke.
                            </p>
                                      <a href="https://doi.org/10.1016/j.envdev.2018.12.001" class="offset-top-10 text-secondary text-justify">
                     <u>https://doi.org/10.1016/j.envdev.2018.12.001</u>
                            </a>
                                        <p class="offset-top-40 text-secondary">
                                           In the contractual Richtersveld National Park (RNP), park officials and neighbouring communities
                                            jointly manage resources, with the aim to harmonize biodiversity conservation and human
                                            land use. Our socio-ecological approach compared herding practices and livelihoods of 36 livestock
                                            owners and 35 hired herders inside and outside RNP, and further assessed soil quality and
                                            vegetation characteristics under different livestock grazing patterns and access to natural resources.
                                            Hired herders were mainly in charge of animal movement patterns but were not included
                                            in formal agreements, which negatively impacted on natural resource management, livelihoods,
                                            animal well-being and communication amongst stakeholders. Soil properties and
                                            vegetation were generally negatively affected through grazing and herding practices in this
                                            fragile semi-arid biodiversity hotspot that encompasses many endangered and endemic species.
                                            Our research highlights the complex social relationships and dynamics between diverse stakeholders
                                            engaged in the contractual park and accentuates the need to improve herders’ social and
                                            economic status.
                                        </p>
                                            <!-- <a href="#" hiddenclass="btn btn-xl btn-primary">get starbis now</a> -->
                                        </div>
                                        <div class="cell-sm-11 cell-md-4 offset-top-40 offset-sm-top-60 offset-md-top-0">
                                            <div class="image-group" style="display:block;">
                                                    <h4>Film Funded By</h4>
                                                    <img src="../img/logo-6/BMZ2.png" class="logo_set" style="width:250px !important;" alt="">
                                                <h4>Project Partners</h4>
                                                <img style="width:400px !important" src="../img/logo-6/pic5_2.jpeg">
                                                <img style="width:160px !important" src="../img/logo-6/municipality.jpg">
                                                <img style="width:160px !important" src="../img/logo-6/pic4.png">
                                                <img style="width: 230px !important; position: absolute; margin-top: 70px;" src="../img/gobi/im2.png">
                                                <img src="../img/logo-6/im1.png" class="logo_set" style="width:fit-content !important; width: -moz-fit-content !important; margin-top: 0px !important;" alt="">
                                            <h4>Research Funded By</h4>
                                                <img style="width:160px !important" src="../img/gobi/im14.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>`
            },
            {
                color: "#000000",
                svgPath: targetSVG,
                label: "",
                latitude: 45.4189185,
                longitude: 92.658126,
                scale: 1.5,
                title: "Great Gobi 'B' SPA, Mongolia.",
                description: `<section id="section-start-journey" class="section-50 section-sm-90 section-lg-top-40 section-lg-bottom-50">
                <div class="shell">
                    <div class="range range-md-top range-sm-center">
                    <div class="cell-sm-10 cell-md-3">

                        <div data-wow-duration="1s" data-wow-offset="200" class="holder cell-sm-10 cell-md-3 wow  bg-gray-lighter">
                            <button data-modal="#modal-html5-video-7" class="btn_set11">
                                <img style="width:100% !important; height:100% !important" src="../img/thumb/thumb_6.png">
                            </button>

                        </div>
                        <div data-wow-duration="1s" data-wow-offset="200" class="holder cell-sm-10 cell-md-3 wow  bg-gray-lighter">
                            <button data-modal="#modal-html5-video-8" class="btn_set11">
                                <img style="width:100% !important; height:100% !important" src="../img/thumb/thumb_7.png">
                            </button>
                        </div>
                        </div>
                        <div class="cell-sm-10 cell-md-5">
                            <!-- <h2 class="text-red-orange">Most Flexible w</h2>-->
                            <p class="h5 offset-top-0">Adapting to change co-management scenarios for local pastoralists and protected are management to maintain biological and cultural diversity in the Dzungarian Gobi in Mongolia
                            </p>
                            <p class="offset-top-10 text-secondary text-justify">
                           <u> Lena M. Michler</u>; Anna C. Treydte; Petra Kaczensky
                            </p>
                            <p class="offset-top-40 text-secondary text-justify">
                            The population of Mongolia is mainly dependent on livestock production and healthy rangelands, but little is known about their grazing strategies, in particular in protected areas. This study combines an interdisciplinary socio-economic and ecological approach to understand pastoralist’s grazing strategies in relation to a co-management approach between the local pastoralists and the management of the Great Gobi B Strictly Protected Area (SPA) in Mongolia. We further aim at developing user-friendly management strategies for sustainable herding practices, especially under increased temperatures, changes in precipitation and higher probability of extreme weather events, as well as changes in land use allowances. The attitude of the local people towards co-management practices will be investigated, as well as the socio-economic aspect of the contribution of livestock grazing to the overall livelihood security of local pastoralists with seasonal access to the Great Gobi B SPA. The involvement of local people in the management of national parks and protected areas through official contracts is still a rarely applied but a highly promising approach. These “contractual parks” as Gobi B are a sustainable way of allowing access and resource use in areas that are important for conservation.
                            </p>
                            <!-- <a href="#" hiddenclass="btn btn-xl btn-primary">get starbis now</a> -->
                        </div>
                        <div class="cell-sm-11 cell-md-4 offset-top-40 offset-sm-top-60 offset-md-top-0">
                            <div class="image-group" style="display:block;">
                                <h4>Project Partners</h4>
                                <img style="width:160px !important" src="../img/gobi/im1.png">
                                <img style="width:160px !important" src="../img/gobi/im2.png">
                                <img style="width:130px !important" src="../img/gobi/im4.png">
                                <img style="width:170px !important" src="../img/gobi/im5.png">
                                   <img style="width:160px !important" src="../img/gobi/im6.png">
                                    <img style="width:160px !important" src="../img/gobi/im3.png">
                                    <h4>Research Funded By</h4>
                                 <img style="width:160px !important" src="../img/gobi/im7.png">
                                <img style="width:160px !important" src="../img/gobi/im8.png">
                            </div>
                        </div>
                    </div>
                </div>
            </section>`
            },
            {
                color: "#000000",
                svgPath: targetSVG,
                label: "",
                latitude: 35.7943835,
                longitude: 74.0459084,
                scale: 1.5,
                title: "Gilgit Batistan, Pakistan.",
                description: `<section id="section-start-journey" class="section-50 section-sm-90 section-lg-top-40 section-lg-bottom-50">
                                <div class="shell">
                                    <div class="range range-md-top range-sm-center">
                                        <div class="cell-sm-10 cell-md-6">
                                            <!-- <h2 class="text-red-orange">Most Flexible w</h2>
                                            <p class="h3 offset-top-0">Bootstrap theme</p> -->
                                            <p class="offset-top-40 text-secondary ">
                                           For project information on Gilgit, Pakistan, please contact <a href="mailto:info@sharingback.org">info@sharingback.org</a>
                                            </p>
                                            <!-- <a href="#" hiddenclass="btn btn-xl btn-primary">get starbis now</a> -->
                                        </div>
                                    </div>
                                </div>
                            </section>`
            },
            {
                color: "#000000",
                svgPath: targetSVG,
                label: "",
                latitude: 47.5680628,
                longitude: 7.9430422,
                scale: 1.5,
                title: "Bad Säckingen, Germany.",
                description: `<section id="section-start-journey" class="section-50 section-sm-90 section-lg-top-40 section-lg-bottom-50">
                                <div class="shell">
                                    <div class="range range-md-top range-sm-center">
                                        <div class="cell-sm-10 cell-md-6">
                                            <!-- <h2 class="text-red-orange">Most Flexible w</h2>
                                            <p class="h3 offset-top-0">Bootstrap theme</p> -->
                                            <p class="offset-top-40 text-secondary">
                                                For more information, please contact <a href="mailto:info@sharingback.org">info@sharingback.org</a>
                                               <br> Head Office <br>
                                                Rebbergweg 3, 79713 Bad Säckingen, Germany.
                                            </p>
                                            <!-- <a href="#" hiddenclass="btn btn-xl btn-primary">get starbis now</a> -->
                                        </div>
                                    </div>
                                </div>
                            </section>`
            }
        ]
    },

    areasSettings: {
        unlistedAreasColor: "#FFCC00"
    },

    imagesSettings: {
        color: "#CC0000",
        rollOverColor: "#CC0000",
        selectedColor: "#000000",
        balloonText: "<strong>[[title]]</strong>"
    }
});

map.addListener("clickMapObject", function(event) {
    map.closeAllDescriptions();
    $("#ProjectDetails").empty();
    $("#ProjectDetails").append(event.mapObject.description);
    console.log(event);
});
