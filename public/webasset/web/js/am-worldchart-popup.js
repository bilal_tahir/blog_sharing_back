var map = AmCharts.makeChart("chartdiv", {
    "type": "map",
    "dataProvider": {
        "map": "worldLow",
        "areas": [{
            "id": "US",
            "modalUrl": "https://en.wikipedia.org/wiki/United_States",
            "selectable": true
        }, {
            "id": "FR",
            "modalUrl": "https://en.wikipedia.org/wiki/France",
            "selectable": true
        }, {
            "id": "CN",
            "modalUrl": "https://en.wikipedia.org/wiki/China",
            "selectable": true
        }],
        "images": [{
            "latitude": 40.3951,
            "longitude": -73.5619,
            "type": "circle",
            "color": "#6c00ff"
        }]
    },
    "areasSettings": {
        "selectedColor": "#CC0000"
    },
    "listeners": [{
        "event": "clickMapObject",
        "method": function (event) {
            console.log("event", event);
            $.fancybox({
                "href": event.mapObject.images,
                "title": event.mapObject.title,
                "type": "iframe"
            });
        }
    }]
});
